import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './product.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private http: HttpClient) { }

  getAttributes(aisle: number, bin: number): Observable<string[]>
  {
    return this.http.get<string[]>(`http://192.168.13.156:3000/products/attributes`);
  }

  getProducts(aisle: number, bin: number): Observable<Product[]>
  {
    return this.http.get<Product[]>(`http://192.168.13.156:3000/products/${aisle}/${bin}`);
  }

  postProducts(product: Product)
  {
    console.log(product);
    this.http.post(`http://192.168.13.156:3000/products/`, product).subscribe(err => console.log(err)); 
  }

  updateProducts(product: Product)
  {
    console.log(product);
    return this.http.put(`http://192.168.13.156:3000/products/${product._id}}`, product);
  }  

  deleteProducts(product: Product) : Observable<string>
  {
    return this.http.delete<string>(`http://192.168.13.156:3000/products/${product._id}`);
  }  
}
