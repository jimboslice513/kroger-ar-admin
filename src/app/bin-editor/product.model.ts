export class Product {
    _id: number;
    name: string;
    description: string;
    aisle: number;
    image_id: number;
    bin: number;
    attributes: string[];
    location: {
      x: number;
      y: number;
      z: number;
    };
    coor: {
        x: number;
        y: number;
        z: number;
    };
}