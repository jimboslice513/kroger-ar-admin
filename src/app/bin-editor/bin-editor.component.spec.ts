import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinEditorComponent } from './bin-editor.component';

describe('BinEditorComponent', () => {
  let component: BinEditorComponent;
  let fixture: ComponentFixture<BinEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
