import { Component, OnInit } from '@angular/core';
import { Product } from './product.model';
import { ProductServiceService } from './product-service.service';
import { ThrowStmt } from '@angular/compiler';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-bin-editor',
  templateUrl: './bin-editor.component.html',
  styleUrls: ['./bin-editor.component.css']
})
export class BinEditorComponent implements OnInit {

  private Bin: number = 0;
  private Aisle: number = 0;
  private Products: Product[][] = [];
  private Bins: number[] = [0, 1, 2];
  private Aisles: number[] = [0, 1, 2];
  private selectedProduct: Product;

  private selectedX: number = 0;
  private selectedY: number = 0; 

  private dropdownList = [];
  private selectedItems = [];;
  private dropdownSettings: any = {};

  constructor(private productService: ProductServiceService) { }

  ngOnInit() {
    this.resetGrid();
    this.getProducts();
    this.getAttributes();
    
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      disabled: false
    };
  }
  
  resetGrid() {
    for (var i: number = 0; i < 5; i++) {
      this.Products[i] = [];
      for (var j: number = 0; j < 4; j++) {
        this.Products[i][j] = new Product();
        this.Products[i][j].name = null;
        this.Products[i][j]._id = null;
      }
    }

    this.selectedProduct = this.Products[this.selectedX][this.selectedY];
  }

  getProducts() {
    console.log("getting products", this.Bin);
    this.resetGrid();
    this.productService.getProducts(this.Aisle, this.Bin).subscribe((products) => {
      products.map((product) => {
        this.Products[product.location.x][3 - product.location.y] = product;
      });
    });
  }

  
  getAttributes() {
    this.productService.getAttributes(this.Aisle, this.Bin).subscribe((attributes) => {
        this.dropdownList = attributes;
    });
  }


  cellClicked(x, y) {
    console.log(x,y);
    this.selectedX = x;
    this.selectedY = y;
    this.selectedProduct = this.Products[x][y];
    this.selectedItems = this.selectedProduct.attributes;
  }

  add(){
    console.log(this.selectedProduct);
    this.selectedProduct.bin = this.Bin;
    this.selectedProduct.aisle = this.Aisle;
    this.selectedProduct.location = { x: this.selectedX, y: 3 - this.selectedY, z: -1};
    this.selectedProduct.attributes = this.selectedItems;
    this.productService.postProducts(this.selectedProduct);
  }

  update() {
    this.productService.updateProducts(this.selectedProduct).subscribe(() =>{
      this.resetGrid();
      this.getProducts();
    })
  }

  delete(){
    this.productService.deleteProducts(this.selectedProduct).subscribe(() =>{
      console.log("refreshing grid");
      this.resetGrid();
      this.getProducts();
    })
  }
}
